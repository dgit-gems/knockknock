## TODO
  - [ ] Add a generator for ErrorSerializer Class
  - [ ] Add generators for controllers - tokens, registrations
  - [ ] Confirmable module
  - [ ] Database Authenticatable module - attr_reader :current_password etc
  - [ ] Send Emails
  - [ ] React UI
  - [ ]

## DONE
  - [x] fast_jsonapi integration for responses
