require 'jwt'

module Knockknock
  class AuthToken
    attr_reader :token
    attr_reader :payload

    def initialize(payload: {}, token: nil, verify_options: {})
      if token.present?
        begin
          @payload, _ = JWT.decode token.to_s, decode_key, true, options.merge(verify_options)
        rescue JWT::ExpiredSignature => e
          raise Knockknock.token_expired_exception_class.new
        rescue JWT::DecodeError
          raise Knockknock.not_found_exception_class.new
        end
        @token = token
      else
        @payload = claims.merge(payload)
        @token = JWT.encode @payload, secret_key, Knockknock.token_signature_algorithm
      end
      @payload = ActiveSupport::HashWithIndifferentAccess.new(@payload)
    end

    def entity_for(entity_class)
      return entity_class.from_token_payload(payload) if entity_class.respond_to? :from_token_payload
      return entity_class.find @payload["sub"]
    end

    def as_json
      { jwt: @token }
    end

    def to_json(options={})
      as_json.to_json
    end

    protected
    def secret_key
      Knockknock.token_secret_signature_key.call
    end

    def decode_key
      Knockknock.token_public_key || secret_key
    end

    def options
      verify_claims.merge({
        algorithm: Knockknock.token_signature_algorithm
      })
    end

    def claims
      _claims = {}
      _claims[:exp] = token_lifetime if verify_lifetime?
      _claims[:aud] = token_audience if verify_audience?
      _claims
    end

    def token_lifetime
      Knockknock.token_lifetime.from_now.to_i if verify_lifetime?
    end

    def verify_lifetime?
      !Knockknock.token_lifetime.nil?
    end

    def verify_claims
      {
        aud: token_audience,
        verify_aud: verify_audience?,
        verify_expiration: verify_lifetime?
      }
    end

    def token_audience
      verify_audience? && Knockknock.token_audience.call
    end

    def verify_audience?
      Knockknock.token_audience.present?
    end

  end
end
