module Knockknock
  class ApplicationController < ActionController::Base
    # protect_from_forgery with: :exception
    wrap_parameters false
    skip_before_action :verify_authenticity_token
    rescue_from Knockknock.not_found_exception_class, with: :handle_not_found
    rescue_from Knockknock.token_expired_exception_class, with: :handle_expired_token
    rescue_from ActionController::ParameterMissing, with: :handle_missing_parameters

    def require_no_authentication
      return true if current_entity.blank?
      render json: { errors: [{ title: 'Already Authenticated' }] }, status: :bad_request
    end

    def authenticate_entity!
      send("authenticate_#{entity_name.singularize}!")
    end

    def current_entity
      send("current_#{entity_name.singularize}")
    end

    protected
    def handle_not_found(exception)
      Rails.logger.error [exception, *exception.backtrace].join($/)
      head :not_found
    end

    def handle_missing_parameters(exception)
      Rails.logger.error [exception, *exception.backtrace].join($/)
      head :bad_request
    end

    def handle_expired_token(exception)
      Rails.logger.error [exception, *exception.backtrace].join($/)
      render json: { status: "token-expired" }, status: :unauthorized
    end

    def entity_class
      entity_class_name.constantize
    end

    def entity_class_name
      entity_name.to_s.classify
    end

    def entity_name
      return request_entity_class_name.to_s.classify.underscore if request_entity_class_name.present?
      return request.env['knockknock.entity'].to_s.classify.underscore
    end

    def request_entity_class_name
      request.env["knockknock.entity_class_name"]
    end
  end
end
