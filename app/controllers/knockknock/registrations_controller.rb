module Knockknock
  class RegistrationsController < ApplicationController
    prepend_before_action :require_no_authentication, only: [:create]
    prepend_before_action :authenticate_entity!, only: [:update]

    # For creating a new user
    def create
      @entity = entity_class.new(register_params)
      yield @entity if block_given?

      if @entity.save
        render_create_success
      else
        render_create_error
      end
    end

    # We need to use a copy of the resource because we don't want to change
    # the current_entity in place.
    # TODO handle confirmable module
    def update
      @entity = Knockknock::AuthToken.new(token: bearer_token).entity_for(entity_class)
      assign_attributes(account_update_params)
      if @entity.errors.added?(:current_password, :invalid) or @entity.errors.added?(:current_password, :blank)
        render_update_error
        return
      end
      yield @entity if block_given?
      if @entity.save
        render_update_success
      else
        render_update_error
      end
    end

    protected

    def register_params
      params.require(entity_name.to_sym).permit(:password, :password_confirmation)
    end

    def account_update_params
      params.require(entity_name.to_sym).permit(:password, :password_confirmation, :current_password)
    end

    def serializer_class_name
      "#{entity_class_name}Serializer"
    end

    def error_serializer_class_name
      "ErrorSerializer"
    end

    def serializer_class_exists?
      class_exists?(serializer_class_name)
    end

    def error_serializer_class_exists?
      class_exists?(error_serializer_class_name)
    end

    def class_exists?(class_name)
      Object.const_defined?(class_name)
    end

    def serializer_class
      serializer_class_name.constantize
    end

    def error_serializer_class
      error_serializer_class_name.constantize
    end

    def assign_attributes(params)
      current_password = params.delete(:current_password)
      if params[:password].blank?
        params.delete(:password)
        params.delete(:password_confirmation) if params[:password_confirmation].blank?
      end

      @entity.assign_attributes(params)
      @entity.valid?
      if not current_entity.authenticate(current_password)
        @entity.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
      end
    end

    def render_create_success
      if serializer_class_exists?
        params = { include_jwt: true }
        params = @entity.register_serializer_params if @entity.respond_to?(:register_serializer_params)
        render json: serializer_class.new(@entity, params: params), status: :created
      else
        render json: { type: entity_name, id: @entity.id }, status: :created
      end
    end

    def render_create_error
      if error_serializer_class_exists?
        render json: error_serializer_class.new(@entity.errors), status: :unprocessable_entity
      else
        render json: { errors: @entity.errors }, status: :unprocessable_entity
      end
    end

    def render_update_success
      if serializer_class_exists?
        render json: serializer_class.new(@entity), status: :ok
      else
        render json: { type: entity_name, id: @entity.id }, status: :ok
      end
    end

    def render_update_error
      if error_serializer_class_exists?
        render json: error_serializer_class.new(@entity.errors), status: :unprocessable_entity
      else
        render json: { errors: @entity.errors }, status: :unprocessable_entity
      end
    end
  end
end
