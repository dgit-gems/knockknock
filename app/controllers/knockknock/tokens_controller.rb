require 'jwt'
module Knockknock
  class TokensController < ApplicationController
    before_action :ensure_entity_present
    before_action :ensure_correct_credentials

    def create
      render json: auth_token, status: :created
    end

    protected

    def ensure_entity_present
      raise Knockknock.not_found_exception_class if not entity.present?
    end

    def ensure_correct_credentials
      # TODO remove hardcoded password, make it configurable
      raise Knockknock.not_found_exception_class if not entity.authenticate(auth_params[:password])
    end

    def auth_token
      return AuthToken.new payload: entity.to_token_payload if entity.respond_to? :to_token_payload
      return AuthToken.new payload: { sub: entity.id }
    end

    def entity
      return @entity if @entity
      return (@entity = entity_class.from_token_request(request)) if entity_class.respond_to? :from_token_request
      return @entity = entity_class.find_by(email: auth_params[:email])
    end

    def auth_params
      params.require(:auth).permit :email, :password
    end
  end
end
