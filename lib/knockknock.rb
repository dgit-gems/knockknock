module Knockknock
  module Controllers
    autoload :Helpers, 'knockknock/controllers/helpers'
  end
end

module Knockknock

  mattr_accessor :token_lifetime
  self.token_lifetime = 1.day

  mattr_accessor :token_audience
  self.token_audience = nil

  mattr_accessor :token_signature_algorithm
  self.token_signature_algorithm = 'HS256'

  mattr_accessor :token_secret_signature_key
  self.token_secret_signature_key = -> { Rails.application.credentials.secret_key_base }

  mattr_accessor :token_public_key
  self.token_public_key = nil

  mattr_accessor :not_found_exception_class_name
  self.not_found_exception_class_name = 'ActiveRecord::RecordNotFound'

  mattr_accessor :token_expired_exception_class_name
  self.token_expired_exception_class_name = 'JWT::ExpiredSignature'

  def self.not_found_exception_class
    not_found_exception_class_name.to_s.constantize
  end

  def self.token_expired_exception_class
    token_expired_exception_class_name.to_s.constantize
  end

  # Default way to setup Knockknock. Run `rails generate knockknock:install` to create
  # a fresh initializer with all configuration values.
  def self.setup
    yield self
  end

  def self.add_mapping(entity, options)
    mapping = Knockknock::Mapping.new(entity, options)
    Knockknock::Controllers::Helpers.define_helpers(mapping)
    mapping
  end

end

require "knockknock/mapping"
require "knockknock/rails"
