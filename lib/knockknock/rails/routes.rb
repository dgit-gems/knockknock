module ActionDispatch::Routing
  class Mapper
    def knockknock_for(*entities)
      options = entities.extract_options!
      options[:as]             ||= @scope[:as]     if @scope[:as].present?
      options[:module]         ||= @scope[:module] if @scope[:module].present?
      options[:path_prefix]    ||= @scope[:path]   if @scope[:path].present?
      options[:path_names]       = (@scope[:path_names] || {}).merge(options[:path_names] || {})
      options[:constraints]      = (@scope[:constraints] || {}).merge(options[:constraints] || {})
      options[:defaults]         = (@scope[:defaults] || {}).merge(options[:defaults] || {})
      options[:options]          = @scope[:options] || {}
      options[:options][:format] = false if options[:format] == false

      entities.map!(&:to_sym)

      entities.each do |entity|

        mapping = Knockknock.add_mapping(entity, options) rescue next

        knockknock_scope(entity, options) do
          with_knockknock_exclusive_scope(mapping.fullpath, mapping.name, options) do
            resource :token, only: [], controller: mapping.controllers[:tokens], path: "" do
              post :create, path: mapping.path_names[:login]
            end
            resource :registration, only: [], controller: mapping.controllers[:registrations], path: "" do
              post :create, path: mapping.path_names[:register]
              put :update, path: mapping.path_names[:account_update]
            end
          end
        end
      end
    end

    def knockknock_scope(entity, options)
      constraint = lambda do |request|
        request.env["knockknock.entity"] = entity
        request.env["knockknock.entity_class_name"] = options[:class_name] if options[:class_name].present?
        true
      end
      constraints(constraint) do
        yield
      end
    end

    protected

    def with_knockknock_exclusive_scope(new_path, new_as, options) #:nodoc:
      current_scope = @scope.dup

      exclusive = { as: new_as, path: new_path, module: nil }
      exclusive.merge!(options.slice(:constraints, :defaults, :options))

      if @scope.respond_to? :new
        @scope = @scope.new exclusive
      else
        exclusive.each_pair { |key, value| @scope[key] = value }
      end
      yield
    ensure
      @scope = current_scope
    end
  end
end
