require "knockknock/rails/routes"
module Knockknock
  class Engine < ::Rails::Engine
    isolate_namespace Knockknock

    initializer "knockknock.helpers" do
      ActiveSupport.on_load(:action_controller) do
        include Knockknock::Controllers::Helpers
      end
    end
  end
end
