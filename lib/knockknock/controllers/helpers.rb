module Knockknock
  module Controllers
    module Helpers
      extend ActiveSupport::Concern

      included do
        # rescue_from Knockknock.not_found_exception_class, with: :not_found

        # def not_found
        #   head :not_found
        # end

        private

        def authorization_header
          request.headers['Authorization']
        end

        def bearer_token
          authorization_header.to_s.split.last
        end
      end

      def self.define_helpers(mapping)
        entity = mapping.name
        # puts "The entity is #{entity}, inspected: #{entity.inspect}"
        class_eval <<-METHODS, __FILE__, __LINE__ + 1

          def authenticate_#{entity}!
            if authorization_header.blank?
              render json: { errors: [{ title: 'Authorization Header missing' }] }, status: :unauthorized
              return false
            end

            if bearer_token.blank?
              render json: { errors: [{ title: 'Authorization Header missing' }] }, status: :unauthorized
              return false
            end

            if current_#{entity}.blank?
              render json: { errors: [{ title: 'Unauthorized' }] }, status: :unauthorized
              return false
            end

            return true
          end

          def current_#{entity}
            begin
              @current_#{entity} ||= Knockknock::AuthToken.new(token: bearer_token).entity_for(#{mapping.class_name})
            rescue Knockknock.not_found_exception_class
              @current_#{entity} = nil
            end
          end

          def #{entity}_signed_in?
            !!current_#{entity}
          end
        METHODS

        ActiveSupport.on_load(:action_controller) do
          if respond_to?(:helper_method)
            helper_method "current_#{entity}", "#{entity}_signed_in?"
          end
        end
      end
    end
  end
end
