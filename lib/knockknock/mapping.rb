module Knockknock
  class Mapping

    attr_reader :singular, :scoped_path, :path, :controllers, :path_names,
                :class_name, :klass

    alias name singular

    def initialize(name, options) #:nodoc:
      @scoped_path = options[:as].present? ? "#{options[:as]}/#{name}" : name.to_s
      @singular = (options[:singular] || @scoped_path.tr('/', '_').singularize).to_sym

      @class_name = (options[:class_name] || name.to_s.classify).to_s
      begin
        @klass = ActiveSupport::Dependencies.constantize(@class_name)
      rescue NameError => e
        raise if @class_name != name.to_s.classify

        Rails.logger.warn "[WARNING] You provided knockknock_for #{name.inspect} but there is " \
          "no model #{name.to_s.classify} defined in your application"
      end

      @path = (options[:path] || name).to_s
      @path_prefix = options[:path_prefix]

      default_controllers(options)
      default_path_names(options)
    end

    def fullpath
      "/#{@path_prefix}/#{@path}".squeeze("/")
    end

    private

    def default_controllers(options)
      mod = options[:module] || "knockknock"
      @controllers = Hash.new { |h, k| h[k] = "#{mod}/#{k}" }
      @controllers.merge!(options[:controllers]) if options[:controllers]
      @controllers.each { |k, v| @controllers[k] = v.to_s }
    end

    def default_path_names(options)
      @path_names = Hash.new { |h, k| h[k] = k.to_s }
      @path_names[:registration] = ""
      @path_names.merge!(options[:path_names]) if options[:path_names]
    end

  end
end
